//Code for drawing art on canvas on my blog
//See it here in action: 
(function(){
	this.ctr = 0;
	this.acDegToRad = function(deg){
			return deg* (-(Math.PI / 180.0));    
		}
	this.drawoncanvas = function(cnvs,piece){
    var ctx = cnvs.getContext("2d");
		
	ctx.clearRect(0,0,300,350);	
	//save the initial state of the context
	ctx.save();		
	//set fill color to gray
	
	ctx.fillStyle = "rgb(110,110,110)";
	//save the current state with fillcolor
	ctx.save();

	//draw 2's base rectangle
	ctx.fillRect(20,200,120,35);
	//bring origin to 2's base
	ctx.translate(20,200);
	//rotate the canvas 35 deg anti-clockwise
	ctx.rotate(acDegToRad(35));
	//draw 2's slant rectangle
	ctx.fillRect(0,0,100,35);
	//restore the canvas to reset transforms
	ctx.restore();
	//set stroke color width and draw the 2's top semi circle
	ctx.strokeStyle = "rgb(110,110,110)";
	ctx.lineWidth = 35;
	ctx.beginPath();
	ctx.arc(77,135,40,acDegToRad(-40),acDegToRad(180),true);
	ctx.stroke();

	//reset canvas transforms
	//ctx.restore();

	//change color to blue
	ctx.fillStyle = "rgb(0,160,212)";
	//save current state of canvas
	ctx.save();
	//draw long dividing rectangle 
	ctx.fillRect(162,20,8,300);
	//draw player head circle
	ctx.beginPath();
	ctx.arc(225,80,35,acDegToRad(0),acDegToRad(360));
	ctx.fill();

	//start new path for tummy :)
	ctx.beginPath();
	ctx.moveTo(170,90);
	ctx.lineTo(230,140);
	ctx.lineTo(170,210);
	ctx.fill();

	//start new path for hand
	//set lineCap and lineJoin to "round", blue color 
	//for stroke, and width of 25px
	ctx.lineWidth = 25;
	ctx.lineCap = "round";
	ctx.strokeStyle = "rgb(0,160,212)";
	ctx.lineJoin = "round";
	ctx.beginPath();
	ctx.moveTo(222,150);
	ctx.lineTo(230,190);
	ctx.lineTo(270,220);
	ctx.stroke();

	ctx.beginPath();
	ctx.moveTo(170, 200);
	ctx.lineTo(250, 260);
	ctx.lineTo(170,320);
	ctx.lineTo(170,200);
	ctx.clip();
	

	//begin new path for drawing leg
	ctx.beginPath();
	ctx.moveTo(160,210);
	ctx.lineTo(195,260);
	ctx.lineTo(160,290);
	ctx.stroke();
	ctx.restore();

	//restore the context back to its initial state
	ctx.restore();
	
/*--------------------------------------------------------*/


	switch(piece)
	{
		//2base
		case 1:
		ctx.save();
		ctx.fillStyle = "rgba(256,256,256,0.75)";
		ctx.fillRect(0,0,300,350);
		ctx.fillStyle = "rgb(110,110,110)";
		ctx.fillRect(20,200,120,35);
		ctx.restore();	
		break;
		
		//2 slant	
		case 2: 
		ctx.save();
		ctx.fillStyle = "rgba(256,256,256,0.75)";
		ctx.fillRect(0,0,300,350);
		ctx.fillStyle = "rgb(110,110,110)";
		ctx.translate(20,200);
		ctx.rotate(acDegToRad(35));
		ctx.fillRect(0,0,100,35);
		ctx.restore();
		break;
		
		//2arc
		case 3:
		ctx.save();
		ctx.fillStyle = "rgba(256,256,256,0.75)";
		ctx.fillRect(0,0,300,350);	
		ctx.strokeStyle = "rgb(110,110,110)";
		ctx.lineWidth = 35;
		ctx.beginPath();
		ctx.arc(77,135,40,acDegToRad(-40),acDegToRad(180),true);
		ctx.stroke();
		ctx.restore();
		break;
		
		//dividing line
		case 4:
		ctx.save();
		ctx.fillStyle = "rgba(256,256,256,0.75)";
		ctx.fillRect(0,0,300,350);	
		ctx.fillStyle = "rgb(0,160,212)";
		ctx.fillRect(162,20,8,300);		
		ctx.restore();
		break;
		
		//head
		case 5:
		ctx.save();
		ctx.fillStyle = "rgba(256,256,256,0.75)";
		ctx.fillRect(0,0,300,350);
		ctx.fillStyle = "rgb(0,160,212)";
		ctx.beginPath();
		ctx.arc(225,80,35,acDegToRad(0),acDegToRad(360));
		ctx.fill();		
		ctx.restore();
		break;
		
		//tummy
		case 6:
		ctx.save();
		ctx.fillStyle = "rgba(256,256,256,0.75)";
		ctx.fillRect(0,0,300,350);	
		ctx.fillStyle = "rgb(0,160,212)";
		ctx.beginPath();
		ctx.moveTo(170,90);
		ctx.lineTo(230,140);
		ctx.lineTo(170,210);
		ctx.fill();
		ctx.restore();
		break;
				
		//hand
		case 7:
		ctx.save();
		ctx.fillStyle = "rgba(256,256,256,0.75)";
		ctx.fillRect(0,0,300,350);	
		ctx.lineWidth = 25;
		ctx.lineCap = "round";
		ctx.strokeStyle = "rgb(0,160,212)";
		ctx.lineJoin = "round";
		ctx.beginPath();
		ctx.moveTo(222,150);
		ctx.lineTo(230,190);
		ctx.lineTo(270,220);
		ctx.stroke();
		ctx.restore();
		break;
		
		//leg
		case 8:
		ctx.save();
		ctx.fillStyle = "rgba(256,256,256,0.75)";
		ctx.fillRect(0,0,300,350);	
		ctx.lineWidth = 25;
		ctx.lineCap = "round";
		ctx.strokeStyle = "rgb(0,160,212)";
		ctx.lineJoin = "round";
		ctx.moveTo(160,210);
		ctx.lineTo(195,260);
		ctx.lineTo(160,290);
		ctx.stroke();		
		ctx.restore();
		break;
		
		//clip leg
		case 9:
		ctx.save();
		ctx.fillStyle = "rgba(256,256,256,0.75)";
		ctx.fillRect(0,0,300,350);	
		ctx.beginPath();
		ctx.moveTo(170, 200);
		ctx.lineTo(250, 260);
		ctx.lineTo(170,320);
		ctx.lineTo(170,200);
		ctx.stroke();
		ctx.beginPath();
		ctx.moveTo(170, 200);
		ctx.lineTo(250, 260);
		ctx.lineTo(170,320);
		ctx.lineTo(170,200);
		ctx.clip();
		ctx.lineWidth = 25;
		ctx.lineCap = "round";
		ctx.strokeStyle = "rgb(0,160,212)";
		ctx.lineJoin = "round";
		ctx.beginPath();
		ctx.moveTo(160,210);
		ctx.lineTo(195,260);
		ctx.lineTo(160,290);
		ctx.stroke();		
		ctx.restore();	
		break;
	}
	if(ctr<10)ctr++;
	else ctr=0;
}
for(var i = 0;i<=10;i++){
this.cnvs = document.getElementById("portalcanvas-"+ i);
if(i<10)
this.drawoncanvas(cnvs,i);
else
setInterval("this.drawoncanvas(cnvs,ctr)",1000);
}
})()